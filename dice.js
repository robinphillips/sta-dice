/*
Star Trek Adventures Dice Roller

MIT License

Copyright (c) 2022 Robin Phillips

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

var sGetRandom = '<i>&hellip; Getting random numbers from Random.org &hellip;</i>'
var errMsg = 'There was an error getting random numbers from random.org. <a href = "https://www.random.org/quota/?format=html" target = "_blank">Check your quota</a>.'

// Cookie code based on https://stackoverflow.com/questions/19189785/is-there-a-good-cookie-library-for-javascript
document.getCookie = function(sName) {
	sName = sName.toLowerCase();
	var oCrumbles = document.cookie.split(';');
	for(var i=0; i<oCrumbles.length;i++)
	{
		var oPair= oCrumbles[i].split('=');
		var sKey = decodeURIComponent(oPair[0].trim().toLowerCase());
		var sValue = oPair.length>1?oPair[1]:'';
		if(sKey == sName)
			return decodeURIComponent(sValue);
	}
	return '';
}
document.setCookie = function(sName,sValue,iDays) {
	if (iDays != 0) {
		var oDate = new Date();
		oDate.setTime (Date.now() + (1000*60*60*24*iDays))
		sExpire = ';expires=' + oDate.toGMTString()
	}
	else
		sExpire = ""
	var sCookie = encodeURIComponent(sName) + '=' + encodeURIComponent(sValue) + sExpire;
	document.cookie= sCookie;
}

function challengeDice (iNumber) {
	// Set results header
	if (iNumber == 1)
		$('#resulth2').text ('Results (1 challenge die)')
	else
		$('#resulth2').text ('Results (' + iNumber + ' challenge dice)')

	// random.org URL
	rndURL = 'https://www.random.org/integers/?num=' + iNumber + '&min=1&max=6&col=1&base=10&format=plain&rnd=new'
	$('#results').html (sGetRandom)
	$.get (rndURL, function (rndNumbers, rndStatus) {
		if (rndStatus == 'success') {
			discordResult = ''
			resultHTML = ''
			iResult = 0
			iEffect = 0
			aNumbers = rndNumbers.split ('\n')

			for (i=0; i<=iNumber; i++) {
				// Ignore blank line
				if (aNumbers [i] != '') {
					resultHTML += '<li data-value="' + aNumbers [i] + '">'

					if (aNumbers [i] <= 2) {
						resultHTML += '0'
					}
					else if (aNumbers [i] == 3) {
						resultHTML += '1'
						iResult++
					}
					else if (aNumbers [i] == 4 || aNumbers [i] == 5) {
						resultHTML += '1 plus effect'
						iResult++
						iEffect++
					}
					else if (aNumbers [i] == 6) {
						resultHTML += '2'
						iResult = iResult + 2
					}

					resultHTML += '\n</li>'
				}
			}
			$('#results').html ('<ol class="olResults">' + resultHTML + '</ol>')
			summaryHTML = 'Summary: ' + iResult
			if (iEffect > 0)
				summaryHTML += ' plus ' + iEffect + ' effect'
			if (iEffect > 1)
				summaryHTML += 's'
			$('#summary').html (summaryHTML)
			// Enable 'Send to Discord' button
			$('#btn_sendtodiscord').attr('disabled',false)
		}
		else {
			$('#results').html (errMsg)
		}

		if ($('#chkSort').prop ('checked')) {
			// Sort checkbox is ticked - sort the results
			sortResults ()
		}
	})
	.fail (function () {
		$('#results').html (errMsg)
	})
}

function d20Dice (iNumber) {
	// Set results header
	$('#resulth2').text ('Results (' + iNumber + 'd20)')

	// random.org URL
	rndURL = 'https://www.random.org/integers/?num=' + iNumber + '&min=1&max=20&col=1&base=10&format=plain&rnd=new'
	$('#results').html (sGetRandom)
	$.get (rndURL, function (rndNumbers, rndStatus) {
		if (rndStatus == 'success') {
			resultHTML = ''
			iResult = 0
			iComplications = 0
			iTarget = parseInt ($('#target').val())
			iDiscipline = parseInt ('0' + $('#discipline').val())
			aNumbers = rndNumbers.split ('\n')

			for (i=0; i<=iNumber; i++) {
				// Ignore blank line
				if (aNumbers [i] != '') {
					// Get number of successes
					if (parseInt (aNumbers [i]) <= iTarget) {
						iResult++
						if (parseInt (aNumbers [i]) == 1) {
							// Natural 1: add an extra success
							iResult++
						}
						else if (parseInt (aNumbers [i]) <= iDiscipline) {
							// Discipline or lower: add an extra success
							iResult++
						}
					}

					if (parseInt (aNumbers [i]) < 10) {
						// Prepend zero so that sort works correctly
						resultHTML += '<li data-value="0' + aNumbers[i] + '">' + aNumbers[i]
						if (parseInt (aNumbers [i]) == 1 || parseInt (aNumbers [i]) <= iDiscipline)
							resultHTML += ' (two successes)'
						else if (parseInt (aNumbers [i]) <= iTarget)
							resultHTML += ' (one success)'
						resultHTML += '\n</li>'
					}
					else {
						resultHTML += '<li data-value="' + aNumbers[i] + '">' + aNumbers[i]
						if (parseInt (aNumbers [i]) >= parseInt ($('#complication').val())) {
							resultHTML += ' (complication)'
							iComplications++
						}
						else if (parseInt (aNumbers [i]) <= iTarget)
							resultHTML += ' (one success)'
						resultHTML += '\n</li>'
					}
				}
			}
			$('#results').html ('<ol class="olResults">' + resultHTML + '</ol>')
		}
		else {
			$('#results').html (errMsg)
		}

		if ($('#chkSort').prop ('checked')) {
			// Sort checkbox is ticked - sort the results
			sortResults ()
		}

		// Summary
		if (iResult == 1)
			summaryResult = 'Summary: 1 success'
		else
			summaryResult = 'Summary: ' + iResult + ' successes'
		if (iComplications == 1)
			summaryResult += ' plus a complication'
		else if (iComplications > 1)
			summaryResult += ' plus ' + iComplications + ' complications'
		if ($('#target').val() != '')
			$('#summary').html (summaryResult)
		else
			$('#summary').html ('')
		// Enable 'Send to Discord' button
		$('#btn_sendtodiscord').attr('disabled',false)
	})
	.fail (function () {
		$('#results').html (errMsg)
	})
}

function sortResults () {
	// Based on https://gist.github.com/StanBoyet/aa6396b69a8ac3d8f12e
	var $results = $('ol.olResults'),
	$resultsli = $results.children ('li')

	$resultsli.sort (function (a,b) {
		var an = a.getAttribute ('data-value'),
			bn = b.getAttribute ('data-value')

		if (an > bn) {
			return 1
		}
		if (an < bn) {
			return -1
		}
		return 0
	})

	$resultsli.detach ().appendTo ($results)
}

function sendToDiscord () {
	// Set cookie with 30 days expiry
	document.setCookie('discordnote', $('#txtDiscordNote').val (), 30)

	discord_msg = ''
	// Set up Discord message: note
	if ($('#txtDiscordNote').val() != '')
		discord_msg += 'Note: _' + $('#txtDiscordNote').val() + '_\n'
	// Set up Discord message: header
	discord_msg += '**' + $('#resulth2').text() + '**\n'
	// Dice rolls
	lines = $('#results').text().split (/\n/)
	for (i in lines) {
		if (lines [i] != '') {
			discord_msg += parseInt (i)+1 + '. ' + lines [i] + '\n'
		}
	}
	// Summary
	if ($('#summary').text() != '')
		discord_msg += '_' + $('#summary').text() + '_'
	// Send message
	var params = {
		content: discord_msg
	}
	var url = discord_webhook_url
	$.ajax({
		url: discord_webhook_url,
		data: JSON.stringify(params),
		contentType: 'application/json',
		type: 'POST',
		success: function () {
			$('#discord_sent').show()
		},
		error: function () {
			$('#discord_error').show()
			console.log ('Error sending to Discord')
			console.log ('discord_webhook_url: ' + discord_webhook_url)
		}
	})
}

$(function () {
	$('.ChallengeDice').click (function () {
		// Disable 'Send to Discord' button
		$('#btn_sendtodiscord').attr('disabled',true)
		// Hide 'Results sent' spans
		$('span.discord_results').hide()
		// Roll dice
		challengeDice ($(this).data ('value'))
	})

	$('.d20Dice').click (function () {
		// Disable 'Send to Discord' button
		$('#btn_sendtodiscord').attr('disabled',true)
		// Hide 'Results sent' spans
		$('span.discord_results').hide()
		// Roll dice
		d20Dice ($(this).data ('value'))
	})

	$('#chkSort').change (function () {
		// Set cookie with 30 days expiry
		document.setCookie('sortresults', $('#chkSort').prop ('checked'), 30)

		if ($('#chkSort').prop ('checked')) {
			// Sort any existing results
			sortResults ()
		}
	})

	$('#btn_sendtodiscord').click (function () {
		sendToDiscord ()
	})

	// Get sort results cookie value and set checkbox accordingly
	if (document.getCookie('sortresults') == 'true') {
		$('#chkSort').prop ('checked', true)
		// Renew the cookie so that it will not expire for another 30 days
		document.setCookie('sortresults', true, 30)
	}
	else
		$('#chkSort').prop ('checked', false)

	// Simple webhook URL checks
	if (typeof discord_webhook_url === 'undefined') {
		console.log ('Discord webhook URL is not set')
	}
	else {
		discord_webhook_url_check = discord_webhook_url.toLowerCase ()
		if (discord_webhook_url_check == '') {
			console.log ('Discord webhook URL is not set')
		}
		else if (discord_webhook_url_check.length < 42) {
			console.log ('Discord webhook URL is too short')
			console.log ('discord_webhook_url: "' + discord_webhook_url + '"')
			// Disable 'Send to Discord' button
			$('#btn_sendtodiscord').attr('disabled',true)
		}
		else if (discord_webhook_url_check.substring (0, 33) != 'https://discord.com/api/webhooks/') {
			console.log ('Discord webhook URL is invalid')
			console.log ('discord_webhook_url: "' + discord_webhook_url + '"')
			// Disable 'Send to Discord' button
			$('#btn_sendtodiscord').attr('disabled',true)
		}
		else {
			console.log ('Discord webhook URL is set and appears to be valid')
			// Show 'Send results to Discord', disable button, set note value
			$('#div_sendtodiscord').show ()
			$('#btn_sendtodiscord').attr('disabled',true)
			$('#txtDiscordNote').val (document.getCookie('discordnote'))
		}
	}
})
