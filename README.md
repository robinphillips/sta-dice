# STA Dice

A web-based dice roller for [Star Trek Adventures](https://www.modiphius.net/pages/star-trek-adventures) by [Modiphius](https://www.modiphius.net/).

## Configuration

To set the configuration, rename or copy `inc_config_dist.js` to `inc_config.js` and edit `inc_config.js`

Currently, the only configuration is a Discord webhook URL. If this is set, results can be sent to a Discord server and channel.

If it is set, a "Send results to Discord" button will be displayed, that will post the dice results to the Discord server and channel related to the webhook.

For details of how to set up a webhook, see <https://support.discord.com/hc/en-us/articles/228383668-Intro-to-Webhooks>

## Licence: MIT License

Copyright (c) 2022 [Robin Phillips](https://rpg.phillipsuk.org/).

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
